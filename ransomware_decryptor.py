import os
from simplecrypt import decrypt

files_to_encrypt = []
master_key = "RANDOM_KEY_BITTE_:D"


def select_all_files():
    global files_to_encrypt
    global master_key

    for root, dirs, files in os.walk("C:\\"):
        for file in files:
            if file.endswith(".enc"):
                decrypt_file(master_key, os.path.join(root, file))
                os.remove(os.path.join(root, file))


def decrypt_file(key, in_filename):
    print("Decrypt {}".format(in_filename))

    out_filename = in_filename.split('.')[0] + "." + in_filename.split('.')[1]

    with open(in_filename) as infile:
        with open(out_filename) as outfile:
            plain_text = decrypt(key, infile.read())
            outfile.write(plain_text)


if __name__ == "__main__":
    select_all_files()
